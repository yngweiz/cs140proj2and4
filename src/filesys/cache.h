#ifndef FILESYS_CACHE_H
#define FILESYS_CACHE_H
#include <inttypes.h>
#include <stdint.h>
#include "threads/synch.h"
/* Size of a disk sector in bytes. */
#define DISK_SECTOR_SIZE 512

/* Index of a disk sector within a disk.
   Good enough for disks up to 2 TB. */
//typedef uint32_t block_sector_t;

/* Format specifier for printf(), e.g.:
   printf ("sector=%"PRDSNu"\n", sector); */
#define PRDSNu PRIu32
#include "threads/thread.h"
#include "threads/malloc.h"

//#include "threads/synch.h"
#include "filesys/inode.h"
#include <string.h>

struct cache_block {
        bool valid, dirty;
        void *upage;
        struct block *d;
        int sec_no;
};

struct lock lock_cache;


struct cache_block cache[64];
int cache_hand;
bool cache_marker[64];

void cache_init (void);
int cache_find( struct block *d, int sec_no);
int cache_set (struct block *d, int sec_no);

void cache_read(struct block *d, int sec_no, void *buffer);
void cache_write(struct block *d, int sec_no, void *buffer);

void cache_write_back(int cache_index);
void cache_done(void);

#endif
