#include "filesys/inode.h"
#include <list.h>
#include <debug.h>
#include <round.h>
#include <string.h>
#include "filesys/filesys.h"
#include "filesys/free-map.h"
#include "threads/malloc.h"
#include "threads/thread.h"
#include "threads/synch.h"
#include "devices/block.h"
/* Identifies an inode. */
#define INODE_MAGIC 0x494e4f44
#define DISK_SECTOR_SIZE 512
static char zeros[DISK_SECTOR_SIZE];
struct lock inode_lock;
/* On-disk inode.
   Must be exactly BLOCK_SECTOR_SIZE bytes long. */

/* Returns the number of sectors to allocate for an inode SIZE
   bytes long. */
struct inode_disk
{
        off_t length;       /* File size in bytes. */
        short sec_no[INODE_PER_SECTOR];
        uint32_t is_dir;
        off_t dir_pos;
        unsigned magic;                     /* Magic number. */
        uint32_t unused[59];               /* Not used. */
};
static inline size_t
bytes_to_sectors (off_t size)
{
  return DIV_ROUND_UP (size, BLOCK_SECTOR_SIZE);
}

/* In-memory inode. */
struct inode 
  {
    struct list_elem elem;              /* Element in inode list. */
    block_sector_t sector;              /* Sector number of disk location. */
    int open_cnt;                       /* Number of openers. */
    bool removed;                       /* True if deleted, false otherwise. */
    int deny_write_cnt;                 /* 0: writes ok, >0: deny writes. */
    struct inode_disk data;             /* Inode content. */
  };

/* Returns the block device sector that contains byte offset POS
   within INODE.
   Returns -1 if INODE does not contain data for a byte at offset
   POS. */

static block_sector_t
byte_to_sector (const struct inode *inode, off_t pos) 
{
        ASSERT (inode != NULL);
        if ( pos >= inode->data.length)
        {
//              printf ( "Byte to sector error %d\n", pos);
                return SECTOR_ERROR;
        }

        int index1 = (pos/512) / INODE_PER_SECTOR;
        int index2 = (pos/512) % INODE_PER_SECTOR;

        ASSERT (inode->data.sec_no[index1] != SECTOR_ERROR);
        struct inode *inode2 = inode_open(inode->data.sec_no[ index1]);
        block_sector_t ret = inode2->data.sec_no[ index2];
        inode_close(inode2);

        ASSERT( ret<100000 || ret == SECTOR_ERROR );

        return ret;
}

/* List of open inodes, so that opening a single inode twice
   returns the same `struct inode'. */
static struct list open_inodes;

/* Initializes the inode module. */
void
inode_init (void) 
{
  list_init (&open_inodes);
  lock_init (&inode_lock);
  lock_init(&lock_ft);
}

/* Initializes an inode with LENGTH bytes of data and
   writes the new inode to sector SECTOR on the file system
   device.
   Returns true if successful.
   Returns false if memory or disk allocation fails. */

bool
inode_create (block_sector_t sector, off_t length)
{
        struct inode_disk *disk_inode = NULL;
        bool success = false;
        ASSERT (length >= 0);

        /* If this assertion fails, the inode structure is not exactly
                 one sector in size, and you should fix that. */
        ASSERT (sizeof *disk_inode == DISK_SECTOR_SIZE);

        size_t sectors = bytes_to_sectors (length); // Total number of sectors.
        disk_inode = calloc (1, sizeof *disk_inode);
        if (disk_inode != NULL)
        {
                disk_inode->length = length;
                disk_inode->is_dir = 0;
                disk_inode->dir_pos = 0;
                disk_inode->magic = INODE_MAGIC;

                int i, j;
                for ( i = 0; i < INODE_PER_SECTOR; i++)
                        disk_inode->sec_no[i] = SECTOR_ERROR;

                for ( i = 0; i < sectors; i+= INODE_PER_SECTOR) {
                        int index1 = i / INODE_PER_SECTOR;
                        int index2 = INODE_PER_SECTOR;
                        if ( sectors - i < INODE_PER_SECTOR)
                                index2 = sectors - i;
                        block_sector_t sec_no;
                        if (free_map_allocate (1, &sec_no)) {
                                disk_inode->sec_no[i/INODE_PER_SECTOR] = sec_no;
                                struct inode_disk indirect;
                                for ( j = 0; j < INODE_PER_SECTOR; j++)
                                        indirect.sec_no[ j] = SECTOR_ERROR;
                                for ( j = 0; j < index2; j++) {
                                        if (free_map_allocate (1, &sec_no)) {
                                                indirect.sec_no[j] = sec_no;
                                                cache_write (fs_device, indirect.sec_no[j], zeros); 
                                        } else {
                                                success = false;
                                                goto DONE;
                                        }
                                } 
                                cache_write (fs_device, disk_inode->sec_no[i/INODE_PER_SECTOR], &indirect);
                        } else {
                                success = false;
                                goto DONE;
                        }
                }
                cache_write (fs_device, sector, disk_inode);
        }
        success = true;

DONE:

        if ( !success) {
                int i, j;
                for ( i = 0; i < sectors; i+=INODE_PER_SECTOR) {
                        if ( disk_inode->sec_no[ i/INODE_PER_SECTOR] == SECTOR_ERROR) 
                                break;
                        struct inode *temp = inode_open (disk_inode->sec_no[i/INODE_PER_SECTOR]);
                        for ( j = 0; j < INODE_PER_SECTOR; j++) {
                                if( temp->data.sec_no[j] == SECTOR_ERROR)
                                        break;
                                free_map_release (temp->data.sec_no[j],1);
                        }
                        inode_close (temp);
                        free_map_release (disk_inode->sec_no[i/INODE_PER_SECTOR], 1);
                }
        }

        free (disk_inode);
        return success;
}
/* Reads an inode from SECTOR
   and returns a `struct inode' that contains it.
   Returns a null pointer if memory allocation fails. */
struct inode *
inode_open (block_sector_t sector) 
{
        struct list_elem *e;
        struct inode *inode;
        bool get_lock_here = false;
	
	if ( !lock_held_by_current_thread (&lock_ft) ) {
	      lock_acquire (&lock_ft);
	      get_lock_here = true;
        }

        /* Check whether this inode is already open. */
        for (e = list_begin (&open_inodes); e != list_end (&open_inodes);
                        e = list_next (e)) 
        {
                inode = list_entry (e, struct inode, elem);
                if (inode->sector == sector) 
                {
                        inode_reopen (inode);
	                if ( get_lock_here)
			     lock_release (&lock_ft);
                        return inode; 
                }
        }

        /* Allocate memory. */
        inode = malloc (sizeof *inode);
        if (inode == NULL) {
	        if ( get_lock_here)
		   lock_release (&lock_ft);
                return NULL;
        }

        /* Initialize. */
        list_push_front (&open_inodes, &inode->elem);
        inode->sector = sector;
        inode->open_cnt = 1;
        inode->deny_write_cnt = 0;
        inode->removed = false;
        cache_read (fs_device, inode->sector, &inode->data);
	if ( get_lock_here)
            lock_release (&lock_ft);
        return inode;
}



/* Reopens and returns INODE. */
        struct inode *
inode_reopen (struct inode *inode)
{
        if (inode != NULL)
                inode->open_cnt++;
        return inode;
}

/* Returns INODE's inode number. */
        block_sector_t
inode_get_inumber (const struct inode *inode)
{
        return inode->sector;
}

/* Closes INODE and writes it to disk.
         If this was the last reference to INODE, frees its memory.
         If INODE was also a removed inode, frees its blocks. */
void
inode_close (struct inode *inode) 
{
        /* Ignore null pointer. */
        if (inode == NULL)
                return;

        /* Release resources if this was the last opener. */
        if (--inode->open_cnt == 0)
        {
                /* Remove from inode list and release lock. */
                list_remove (&inode->elem);

                /* Deallocate blocks if removed. */
                if (inode->removed) 
                {
                        /////
                        int i, j;
                        size_t sectors = bytes_to_sectors (inode->data.length);
                        for (i = 0; i < sectors; i+= INODE_PER_SECTOR) {
                                if ( inode->data.sec_no[ i/INODE_PER_SECTOR] == SECTOR_ERROR) 
                                        break;
                                struct inode *temp = inode_open (inode->data.sec_no[i/INODE_PER_SECTOR]);
                                for ( j = 0; j < INODE_PER_SECTOR; j++) {
                                        if( temp->data.sec_no[j] == SECTOR_ERROR)
                                                break;
                                        free_map_release (temp->data.sec_no[j],1);
                                }
                                inode_close (temp);
                                free_map_release (inode->data.sec_no[i/INODE_PER_SECTOR], 1);
                        }
                        ////

                        free_map_release (inode->sector, 1);
                }
                else{
                        cache_write( fs_device, inode->sector, &inode->data);
                }       

                free (inode); 
        }
}

/* Marks INODE to be deleted when it is closed by the last caller who
         has it open. */
        void
inode_remove (struct inode *inode) 
{
        ASSERT (inode != NULL);
        inode->removed = true;
}

/* Reads SIZE bytes from INODE into BUFFER, starting at position OFFSET.
         Returns the number of bytes actually read, which may be less
         than SIZE if an error occurs or end of file is reached. */
        off_t
inode_read_at (struct inode *inode, void *buffer_, off_t size, off_t offset) 
{
        uint8_t *buffer = buffer_;
        off_t bytes_read = 0;
        uint8_t *bounce = NULL;

        while (size > 0) 
        {

                /* Disk sector to read, starting byte offset within sector. */
                block_sector_t sector_idx = byte_to_sector (inode, offset);
                int sector_ofs = offset % DISK_SECTOR_SIZE;

                /* Bytes left in inode, bytes left in sector, lesser of the two. */
                off_t inode_left = inode_length (inode) - offset;
                int sector_left = DISK_SECTOR_SIZE - sector_ofs;
                int min_left = inode_left < sector_left ? inode_left : sector_left;

                /* Number of bytes to actually copy out of this sector. */
                int chunk_size = size < min_left ? size : min_left;
                if (chunk_size <= 0)
                        break;

                if (sector_ofs == 0 && chunk_size == DISK_SECTOR_SIZE) 
                {
                        /* Read full sector directly into caller's buffer. */
                        cache_read (fs_device, sector_idx, buffer + bytes_read); 

                }
                else 
                {
                        /* Read sector into bounce buffer, then partially copy
                                 into caller's buffer. */
                        if (bounce == NULL) 
                        {
                                bounce = malloc (DISK_SECTOR_SIZE);
                                if (bounce == NULL)
                                        break;
                        }
                        cache_read (fs_device, sector_idx, bounce);
                        memcpy (buffer + bytes_read, bounce + sector_ofs, chunk_size);
                }

                /* Advance. */
                size -= chunk_size;
                offset += chunk_size;
                bytes_read += chunk_size;
        }
        free (bounce);

        return bytes_read;
}

/* Writes SIZE bytes from BUFFER into INODE, starting at OFFSET.
         Returns the number of bytes actually written, which may be
         less than SIZE if end of file is reached or an error occurs.
         (Normally a write at end of file would extend the inode, but
         growth is not yet implemented.) */
        off_t
inode_write_at (struct inode *inode, const void *buffer_, off_t size,
                off_t offset) 
{
        const uint8_t *buffer = buffer_;
        off_t bytes_written = 0;
        uint8_t *bounce = NULL;

        if (inode->deny_write_cnt)
                return 0;


        if( inode->data.length < size + offset )
        {
                int i, j;
                int s_index1 = ((inode->data.length)/512 + 1) / INODE_PER_SECTOR;
                int s_index2 = ((inode->data.length)/512 + 1) % INODE_PER_SECTOR;
                if ( inode->data.length == 0)
                        s_index2 = 0;
                int e_index1 = ((size+offset)/512) / INODE_PER_SECTOR;
                int e_index2 = ((size+offset)/512) % INODE_PER_SECTOR;
//              printf ( "length:%d, size:%d, offset:%d\n", inode->data.length, size, offset);
//              printf ( "si1:%d, si2:%d, ei1:%d, ei2:%d\n", s_index1, s_index2, e_index1, e_index2);

                for ( i = s_index1; i <= e_index1; i++) {
                        int s_index = 0, e_index = INODE_PER_SECTOR - 1;
                        struct inode *inode2 = NULL;

                        if ( i == s_index1)
                                s_index = s_index2;
                        if ( i == e_index1)
                                e_index = e_index2;

                        if ( inode->data.sec_no[ i] == SECTOR_ERROR) {
                                block_sector_t sec_no;
                                if ( free_map_allocate (1, &sec_no)) {
                                        inode->data.sec_no[i] = sec_no;
                                        inode2 = inode_open (inode->data.sec_no[i]);
                                        for ( j = 0; j < INODE_PER_SECTOR; j++)
                                                inode2->data.sec_no[ j] = SECTOR_ERROR;
                                } else {
                                        return 0;
                                }
                        } else {
                                int temp = inode->data.sec_no[i];
                                inode2 = inode_open (inode->data.sec_no[ i]);
                        }
//                      printf ( "i:%d, s_index:%d, e_index:%d\n", i, s_index, e_index);

                        for ( j = s_index; j <= e_index; j++) {
//                              printf ( "i:%d, j:%d, sec:%d\n", i, j, inode2->data.sec_no[j]);
                                ASSERT ( inode2->data.sec_no[ j] == SECTOR_ERROR );
                                block_sector_t sec_no;
                                if ( free_map_allocate (1, &sec_no)) {
                                        inode2->data.sec_no[ j] = sec_no;
                                        cache_write (fs_device, inode2->data.sec_no[j], zeros); 
                                } else {
                                        return 0;
                                }
                        }
                        inode_close (inode2);
                        inode->data.length = size + offset;
                }
        }

        while (size > 0) 
        {
                /* Sector to write, starting byte offset within sector. */
                block_sector_t sector_idx = byte_to_sector (inode, offset);
                ASSERT (sector_idx != SECTOR_ERROR);
                int sector_ofs = offset % DISK_SECTOR_SIZE;

                /* Bytes left in inode, bytes left in sector, lesser of the two. */
                off_t inode_left = inode_length (inode) - offset;
                int sector_left = DISK_SECTOR_SIZE - sector_ofs;
                int min_left = inode_left < sector_left ? inode_left : sector_left;

                /* Number of bytes to actually write into this sector. */
                int chunk_size = size < min_left ? size : min_left;
                if (chunk_size <= 0)
                        break;

                if (sector_ofs == 0 && chunk_size == DISK_SECTOR_SIZE) 
                {
                        /* Write full sector directly to disk. */
                        cache_write (fs_device, sector_idx, buffer + bytes_written); 
                }
                else 
                {
                        /* We need a bounce buffer. */
                        if (bounce == NULL) 
                        {
                                bounce = malloc (DISK_SECTOR_SIZE);
                                if (bounce == NULL)
                                        break;
                        }

                        /* If the sector contains data before or after the chunk
                                 we're writing, then we need to read in the sector
                                 first.  Otherwise we start with a sector of all zeros. */
                        if (sector_ofs > 0 || chunk_size < sector_left) 
                                cache_read (fs_device, sector_idx, bounce);
                        else
                                memset (bounce, 0, DISK_SECTOR_SIZE);
                        memcpy (bounce + sector_ofs, buffer + bytes_written, chunk_size);
                        cache_write (fs_device, sector_idx, bounce); 
                }

                /* Advance. */
                size -= chunk_size;
                offset += chunk_size;
                bytes_written += chunk_size;
        }
        free (bounce);

        return bytes_written;
}

/* Disables writes to INODE.
         May be called at most once per inode opener. */
        void
inode_deny_write (struct inode *inode) 
{
        inode->deny_write_cnt++;
        ASSERT (inode->deny_write_cnt <= inode->open_cnt);
}

/* Re-enables writes to INODE.
         Must be called once by each inode opener who has called
         inode_deny_write() on the inode, before closing the inode. */
        void
inode_allow_write (struct inode *inode) 
{
        ASSERT (inode->deny_write_cnt > 0);
        ASSERT (inode->deny_write_cnt <= inode->open_cnt);
        inode->deny_write_cnt--;
}

/* Returns the length, in bytes, of INODE's data. */
        off_t
inode_length (const struct inode *inode)
{
        return inode->data.length;
}

bool 
inode_is_dir (struct inode *inode) 
{
        return inode->data.is_dir;
}

void
inode_set_is_dir (struct inode *inode)
{
        inode->data.is_dir = 1;
}

int
inode_open_cnt (struct inode *inode)
{
        return inode->open_cnt;
}

int
inode_dir_pos (struct inode *inode)
{
        return inode->data.dir_pos;
}

void
inode_set_dir_pos (struct inode *inode, off_t new_pos)
{
        inode->data.dir_pos = new_pos;
}

void inode_print (void) {

        struct list_elem *e;
        for ( e = list_begin(&open_inodes); e != list_end(&open_inodes);
                        e = list_next(e)) {
                printf ( "Open inodes %d\n", list_entry(e, struct inode, elem)->sector);

        }

}

