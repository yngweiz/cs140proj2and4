#include <list.h>
#include "devices/block.h"
#include "filesys/cache.h"
#include "filesys/inode.h"
#include "filesys/file.h"
#include "threads/malloc.h"
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "threads/synch.h"
#include "filesys/filesys.h"
#include "devices/timer.h"

void cache_init (void) {
        int i;
        for ( i = 0; i < 64; i++) {
                cache[ i].valid = 0;
                cache[ i].dirty = 0;
                cache[ i].upage = malloc(DISK_SECTOR_SIZE);
                cache_marker[ i] = 0;
        }
        cache_hand = 0;
        lock_init (&lock_cache);
}
int cache_find (struct block *d, int sec_no) {
        int i;
        for ( i = 0; i < 64; i++) {
                if ( cache[ i].valid == true && cache[ i].d == d && cache[ i].sec_no == sec_no) {
                        return i;
                }
        }
        return -1;
}
int cache_set (struct block *d, int sec_no) {
        while (true) {
                ASSERT (0<=cache_hand && cache_hand<64);
                if ( cache[ cache_hand].valid == 0)
                        break;
                if ( cache_marker[ cache_hand] == 0) {
                        cache_marker[ cache_hand] = 1;
                } else {
                        break;
                }
                cache_hand++;
                if( cache_hand ==64 )
                        cache_hand = 0;
        }

        ASSERT (0<=cache_hand && cache_hand<64);

        if ( cache[ cache_hand].valid && cache[ cache_hand].dirty)
                cache_write_back (cache_hand);

        cache[ cache_hand].valid = 1;
        cache[ cache_hand].dirty = 0;
        cache[ cache_hand].d = d;
        cache[ cache_hand].sec_no = sec_no;
        cache_marker[ cache_hand] = 0;

        int ret = cache_hand;
        cache_hand = (( cache_hand == 63 )? 0 : cache_hand+1 );
//      printf( "cache_hand : %d %s\n", ret, thread_current()->name );
        return ret;
}
void cache_write_back(int cache_index) {
        int i = cache_index;
        ASSERT( cache[ i].valid && cache[ i].dirty );

        block_write(cache[ i].d, cache[ i].sec_no, cache[ i].upage);
        cache[ i].valid = 0;
        cache_marker[ i] = 0;
}
void cache_read (struct block *d, int sec_no, void *buffer) {
/*      if ( buffer == 0xbfff7f90) {
                char *tmp = 0;
                *tmp = 'a';
        }*/

        bool get_lock_here = true;
        if (lock_held_by_current_thread(&lock_cache))   
                get_lock_here = false;
        else
                lock_acquire(&lock_cache);

        int index = cache_find ( d, sec_no);
        if (index==-1) {
                index = cache_set (d, sec_no);
                block_read (d, sec_no, cache[ index].upage);
        }
//      printf( "cache_read : %d %s\n", index, thread_current() -> name );
        memcpy ( buffer, cache[index].upage, DISK_SECTOR_SIZE);
        if ( get_lock_here)
                lock_release(&lock_cache);
}
void cache_write (struct block *d, int sec_no, void *buffer) {
        bool get_lock_here = true;
        if (lock_held_by_current_thread(&lock_cache))
                get_lock_here = false;
        else
                lock_acquire(&lock_cache);
        int index = cache_find ( d, sec_no);
        if (index==-1)
                index = cache_set (d, sec_no);

//      printf( "cache_write : %d\n", index );
        cache[ index].dirty = 1;
        memcpy ( cache[index].upage, buffer, DISK_SECTOR_SIZE);
        if (get_lock_here)
                lock_release(&lock_cache);
}

void cache_done (void) {
        int i;
        for ( i = 0; i < 64; ++i) {
                if ( cache[ i].valid && cache[ i].dirty)
                        cache_write_back(i);
                free(cache[ i].upage);
        }
}
